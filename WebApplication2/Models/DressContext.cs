﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class DressContext : DbContext
    {
        public DbSet<Dress> Dresses { get; set; }
        public DbSet <Cart> Carts { get; set; }
        public DbSet <Order> Orders { get; set; }
        public DbSet <OrderDetail> OrderDetails { get; set; }
        public DbSet <VacancyViewModel> Vacancies { get; set; }
    }
}