﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Dress
    {
        [Key]
        public int IdDress { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int Size { get; set; }
        public string Colour { get; set; }
        public string Producer { get; set; }
        public int Length { get; set; }
    }
}