﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class VacancyViewModelsController : Controller
    {
        private DressContext db = new DressContext();

        // GET: VacancyViewModels
        public ActionResult Index()
        {
            return View(db.Vacancies.ToList());
        }

        // GET: VacancyViewModels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VacancyViewModel vacancyViewModel = db.Vacancies.Find(id);
            if (vacancyViewModel == null)
            {
                return HttpNotFound();
            }
            return View(vacancyViewModel);
        }

        // GET: VacancyViewModels/Create
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Upload()
        {
            foreach (string file in Request.Files)
            {
                var upload = Request.Files[file];
                if (upload != null)
                {
                    // получаем имя файла
                    string fileName = System.IO.Path.GetFileName(upload.FileName);
                    // сохраняем файл в папку Files в проекте
                    upload.SaveAs(Server.MapPath("~/Files/" + fileName));
                }
            }
            return Json("файл загружен");
        }
        // POST: VacancyViewModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VacancyId,Title,Description,IsActive,VacancyType")] VacancyViewModel vacancyViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Vacancies.Add(vacancyViewModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vacancyViewModel);
        }

        // GET: VacancyViewModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VacancyViewModel vacancyViewModel = db.Vacancies.Find(id);
            if (vacancyViewModel == null)
            {
                return HttpNotFound();
            }
            return View(vacancyViewModel);
        }

        // POST: VacancyViewModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VacancyId,Title,Description,IsActive,VacancyType")] VacancyViewModel vacancyViewModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vacancyViewModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vacancyViewModel);
        }

        // GET: VacancyViewModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VacancyViewModel vacancyViewModel = db.Vacancies.Find(id);
            if (vacancyViewModel == null)
            {
                return HttpNotFound();
            }
            return View(vacancyViewModel);
        }

        // POST: VacancyViewModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VacancyViewModel vacancyViewModel = db.Vacancies.Find(id);
            db.Vacancies.Remove(vacancyViewModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
